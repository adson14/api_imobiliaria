import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProprietarioModule } from './proprietario/proprietario.module';

@Module({
	imports: [ProprietarioModule],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}
