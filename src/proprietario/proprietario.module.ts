import { ErrorMessages } from 'src/utility/error_messages';
import { PrismaService } from './../prisma/prisma.service';
import { Module } from '@nestjs/common';
import { ProprietarioService } from './proprietario.service';
import { ProprietarioController } from './proprietario.controller';

@Module({
	controllers: [ProprietarioController],
	providers: [ProprietarioService, PrismaService, ErrorMessages],
})
export class ProprietarioModule {}
