import { ErrorMessages } from 'src/utility/error_messages';
import { PrismaService } from './../prisma/prisma.service';
import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateProprietarioDto } from './dto/create-proprietario.dto';
import { UpdateProprietarioDto } from './dto/update-proprietario.dto';

@Injectable()
export class ProprietarioService {
	constructor(
		private readonly prisma: PrismaService,
		private erroMessage: ErrorMessages
	) {}

	async create(data: CreateProprietarioDto) {
		try {
			return await this.prisma.proprietario.create({ data });
		} catch (error) {
			var message = await this.erroMessage.trait(error);
			throw new BadRequestException(message);
		}
	}

	findAll() {
		return this.prisma.proprietario.findMany({});
	}

	findOne(id: number) {
		return `This action returns a #${id} proprietario`;
	}

	update(id: number, data: UpdateProprietarioDto) {
		return `This action updates a #${id} proprietario`;
	}

	remove(id: number) {
		return `This action removes a #${id} proprietario`;
	}
}
