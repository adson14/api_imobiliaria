import { Prisma } from '@prisma/client';
export class Proprietario implements Prisma.ProprietarioUncheckedCreateInput {
	id?: number;
	nome: string;
	cpf: string;
	rg: string;
	data_nascimento: string | Date;
	estado_civil: string;
	telefone: string;
	email: string;
	endereco: string;
	numero: string;
	bairro: string;
	cidade: string;
	estado: string;
	imoveis?: Prisma.ImovelUncheckedCreateNestedManyWithoutProprietarioInput;
}
