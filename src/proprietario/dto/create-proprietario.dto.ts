import { IsString, IsDate, IsNotEmpty, IsEmail } from 'class-validator';
import { Proprietario } from './../entities/proprietario.entity';

export class CreateProprietarioDto extends Proprietario {
	@IsString()
	@IsNotEmpty({ message: 'O campo nome não pode ser vazio' })
	nome: string;

	@IsString()
	@IsNotEmpty({ message: 'O campo cpf não pode ser vazio' })
	cpf: string;

	@IsString()
	@IsNotEmpty({ message: 'O campo rg não pode ser vazio' })
	rg: string;

	@IsNotEmpty({ message: 'O campo data_nascimento não pode ser vazio' })
	data_nascimento: string | Date;

	@IsString()
	@IsNotEmpty({ message: 'O campo estado_civil não pode ser vazio' })
	estado_civil: string;

	@IsString()
	@IsNotEmpty({ message: 'O campo telefone não pode ser vazio' })
	telefone: string;

	@IsEmail()
	@IsNotEmpty({ message: 'O campo email não pode ser vazio' })
	email: string;

	@IsString()
	@IsNotEmpty({ message: 'O campo endereco não pode ser vazio' })
	endereco: string;

	@IsString()
	@IsNotEmpty({ message: 'O campo numero não pode ser vazio' })
	numero: string;

	@IsString()
	@IsNotEmpty({ message: 'O campo bairro não pode ser vazio' })
	bairro: string;

	@IsString()
	@IsNotEmpty({ message: 'O campo cidade não pode ser vazio' })
	cidade: string;

	@IsString()
	@IsNotEmpty({ message: 'O campo estado não pode ser vazio' })
	estado: string;
}
