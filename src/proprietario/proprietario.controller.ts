import {
	Controller,
	Get,
	Post,
	Body,
	Patch,
	Param,
	Delete,
} from '@nestjs/common';
import { ProprietarioService } from './proprietario.service';
import { CreateProprietarioDto } from './dto/create-proprietario.dto';
import { UpdateProprietarioDto } from './dto/update-proprietario.dto';
import * as moment from 'moment';
import 'moment/locale/pt-br';

@Controller('proprietario')
export class ProprietarioController {
	constructor(private readonly proprietarioService: ProprietarioService) {}

	@Post()
	create(@Body() createProprietarioDto: CreateProprietarioDto) {
		createProprietarioDto.data_nascimento = moment(
			createProprietarioDto.data_nascimento
		).toISOString();

		return this.proprietarioService.create(createProprietarioDto);
	}

	@Get()
	findAll() {
		return this.proprietarioService.findAll();
	}

	@Get(':id')
	findOne(@Param('id') id: string) {
		return this.proprietarioService.findOne(+id);
	}

	@Patch(':id')
	update(
		@Param('id') id: string,
		@Body() updateProprietarioDto: UpdateProprietarioDto
	) {
		return this.proprietarioService.update(+id, updateProprietarioDto);
	}

	@Delete(':id')
	remove(@Param('id') id: string) {
		return this.proprietarioService.remove(+id);
	}
}
