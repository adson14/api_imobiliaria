import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';

@Injectable()
export class ErrorMessages {
	async trait(error) {
		if (error instanceof Prisma.PrismaClientKnownRequestError) {
			return await this.prismaError(error);
		}

		return false;
	}

	private prismaError(error): string {
		var mensagem = '';
		switch (error.code) {
			case 'P2002':
				mensagem = `${error.meta['target'][0]} já cadastrado `;
				break;
			default:
				mensagem = 'Ocorreu um erro na execução. Tente novamente! ';
				break;
		}

		return mensagem;
	}
}
