import { ValidationPipe, VersioningType } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	// Versioning
	app.enableVersioning({
		type: VersioningType.URI,
		defaultVersion: '1',
		prefix: 'api/v',
	});
	//Utiliza a validação de forma global
	app.useGlobalPipes(new ValidationPipe({ transform: true }));
	await app.listen(3000);
}
bootstrap();
