FROM --platform=linux/amd64 node:16-alpine


#RUN apt-get update
#RUN apt-get install -y openssl

WORKDIR /usr/app

COPY package.json .
#COPY package-lock.json .
COPY prisma ./prisma/
COPY .env ./

RUN npm install

RUN npm install -g @nestjs/cli
RUN npm install -g prisma

#RUN prisma generate

#RUN npx prisma migrate dev --name base-setup init

COPY . .



EXPOSE 3000

CMD ["npm","run","start:dev"]