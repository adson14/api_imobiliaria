-- CreateTable
CREATE TABLE "Proprietario" (
    "id" SERIAL NOT NULL,
    "nome" TEXT NOT NULL,
    "cpf" TEXT NOT NULL,
    "rg" TEXT NOT NULL,
    "data_nascimento" TIMESTAMP(3) NOT NULL,
    "estado_civil" TEXT NOT NULL,
    "telefone" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "endereco" TEXT NOT NULL,
    "numero" TEXT NOT NULL,
    "bairro" TEXT NOT NULL,
    "cidade" TEXT NOT NULL,
    "estado" TEXT NOT NULL,

    CONSTRAINT "Proprietario_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "TipoContrato" (
    "id" SERIAL NOT NULL,
    "nome" TEXT NOT NULL,
    "tipo" TEXT NOT NULL,

    CONSTRAINT "TipoContrato_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Imovel" (
    "id" SERIAL NOT NULL,
    "proprietario_id" INTEGER NOT NULL,
    "tipo_contrato_id" INTEGER NOT NULL,
    "titulo" TEXT NOT NULL,
    "descricao" TEXT,
    "endereco" TEXT NOT NULL,
    "cidade" TEXT NOT NULL,
    "bairro" TEXT NOT NULL,
    "valor" DECIMAL(65,30) NOT NULL,
    "area_total" DOUBLE PRECISION,
    "area_construida" DOUBLE PRECISION,
    "quartos" INTEGER NOT NULL DEFAULT 0,
    "banheiros" INTEGER NOT NULL DEFAULT 0,
    "suites" INTEGER NOT NULL DEFAULT 0,
    "garagens" INTEGER NOT NULL DEFAULT 0,
    "piscinas" INTEGER NOT NULL DEFAULT 0,
    "condicao" TEXT NOT NULL,
    "tipo_contrato" TEXT NOT NULL,
    "comissao" DECIMAL(65,30),

    CONSTRAINT "Imovel_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Imovel" ADD CONSTRAINT "Imovel_proprietario_id_fkey" FOREIGN KEY ("proprietario_id") REFERENCES "Proprietario"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Imovel" ADD CONSTRAINT "Imovel_tipo_contrato_id_fkey" FOREIGN KEY ("tipo_contrato_id") REFERENCES "TipoContrato"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
